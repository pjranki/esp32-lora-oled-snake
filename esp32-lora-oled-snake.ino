#include <Wire.h>
#include <snake.h>
#define WII_EXT_I2C_DEBUG(msg)  Serial.println(msg)
#include <wiiexti2c.h>
#include <vint.h>

// OLED
#include <SSD1306.h>
//OLED pins to ESP32 GPIOs via this connecthin:
//OLED_SDA -- GPIO4
//OLED_SCL -- GPIO15
//OLED_RST -- GPIO16
SSD1306  display(0x3c, 4, 15);

// Snake
Snake snake;

// Wii
WiiExti2c controller;
WiiExtClassic classic;

// Virtual interrupts
vint::Interrupt interrupts;

void setup() {

  // Serial
  Serial.begin(115200);
  while (!Serial);
  Serial.println("Serial ready");

  // OLED
  pinMode(16,OUTPUT);
  digitalWrite(16, LOW);    // set GPIO16 low to reset OLED
  delay(50); 
  digitalWrite(16, HIGH); // while OLED is running, must set GPIO16 in high
  display.init();
  // display.flipScreenVertically();
  display.setContrast(255);

  // Virtual interrupts
  interrupts.set_get_tick_count([]() -> uint32_t {
    return (uint32_t) millis();  // only acurate to 10ms
  });

  // Screen
  uint32_t screen_width = 128;
  uint32_t screen_height = 64;

  // Snake grid 20 x 20
  // 20 * 3 pixels = 60 pixels (screen is 128 x 64 pixels)
  snake.set_width(20); 
  snake.set_height(20);

  // Snake graphics
  snake.set_draw_clear([]() {
    display.clear();
  });
  snake.set_draw_snake([](int16_t x, int16_t y) {
    x = 33 + (x * 3);
    y = 1 + (y * 3);
    display.setColor(WHITE);
    display.fillRect(x, y, 3, 3);
  });
  snake.set_draw_apple([](int16_t x, int16_t y) {
    x = 33 + (x * 3);
    y = 2 + (y * 3);
    display.setColor(WHITE);
    display.drawCircle(x + 1, y, 1);
  });

  // Snake random
  snake.set_random([](int16_t min, int16_t max) -> int16_t {
    return min + (esp_random() % (max + 1));
  });

  // Wii Extension
  controller.set_write([](uint8_t addr, const uint8_t *data, uint8_t size) {
    Wire.setClock(100000);  // ESP32 i2c comms glitch if any higher
    Wire.beginTransmission(addr);
    for (uint8_t i = 0; i < size; i++) {
      Wire.write(data[i]);
    }
    uint8_t error = Wire.endTransmission();
    return error == 0;
  });
  controller.set_read([](uint8_t addr, uint8_t *data, uint8_t size) {
    Wire.setClock(100000);  // ESP32 i2c comms glitch if any higher
    uint8_t recd = Wire.readBytes(data, Wire.requestFrom(addr, size));
    return recd == size;
  });
  controller.set_delay_ms([](uint32_t ms) {
    delay(ms);
  });
  controller.set_data_callback([](WiiExti2c::WiiExtType type, const uint8_t *data, uint8_t size) {
    if (type == WiiExti2c::WiiExtType::kWiiExtClassic) {
      classic.update(data, size);
    }
  });

  // Wii Classic controller buttons for Snake
  classic.set_button_down([](WiiExtButton button) {
    switch(button) {
      case kWiiExtButtonDpadLeft:
        snake.key_left();
        break;
      case kWiiExtButtonDpadUp:
        snake.key_up();
        break;
      case kWiiExtButtonDpadRight:
        snake.key_right();
        break;
      case kWiiExtButtonDpadDown:
        snake.key_down();
        break;
    }
  });

  // Snake frame rate
  interrupts.set_interrupt(1000/15, [](){
    snake.tick();
    display.setColor(WHITE);
    display.drawRect(32, 0, 62, 62);
    display.display();
  });

  // Snake reset
  snake.reset();
}

void loop() {
  interrupts.run_loop();
  controller.run_loop();
}
